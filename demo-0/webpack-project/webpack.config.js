module.exports = {
  entry: "main.js",
  resolve: {
    modulesDirectories: ['src']
  },
  output: {
    path: "dist",
    filename: "bundle.js"
  }
}