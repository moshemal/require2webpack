module.exports = {
  entry: "main.js",
  resolve: {
    modulesDirectories: ['src']
  },
  output: {
    path: "dist",
    filename: "bundle.js"
  },
  resolveLoader: {
    alias: {
      text: 'raw'  
    }
  }
  // module: {
  //   loaders: [
  //     {loader:'raw'},
  //     {loader: 'bundle'}
  //   ]
  // }
  // loaders: [
  // {
  //   test: /\.jsx?$/,
  //   //test: /\.es6.js/,
  //   exclude: /(node_modules|bower_components)/,
  //   loader: 'babel'
  // }
//]
}